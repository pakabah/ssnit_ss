<?php
/**
 * Created by IntelliJ IDEA.
 * User: pakabah
 * Date: 10/04/2016
 * Time: 9:28 PM
 */

require 'vendor/autoload.php';
include 'vendor/includes/GCMPushMessage.php';

$config['displayErrorDetails'] = true;

$config['db']['host']   = "localhost";
$config['db']['user']   = "ssnit_root";
$config['db']['pass']   = "ndvem92";
$config['db']['dbname'] = "ssnit";

$app = new Slim\App(["settings" => $config]);

$container = $app->getContainer();

$container['db'] = function ($c) {
    $db = $c['settings']['db'];
    $pdo = new PDO("mysql:host=" . $db['host'] . ";dbname=" . $db['dbname'],
        $db['user'], $db['pass']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    return $pdo;
};

$app->get('/', function($request,$response,$args){
    $response->write("It Works");
});

$app->post('/getUser', function($request,$response,$args){
    $data = $request->getParsedBody();
    $parsedata = [];
    $parsedata['id'] = filter_var($data['id'], FILTER_SANITIZE_STRING);
    return getUserDetails($parsedata['id'],$this->db);
});

$app->post('/getRecords', function($request,$response,$args){
    $data = $request->getParsedBody();
    $parsedata = [];
    $parsedata['id'] = filter_var($data['id'], FILTER_SANITIZE_STRING);
    return getRecords($parsedata['id'],$this->db);
});

$app->post('/getMapInfo', function($request,$response,$args){
    return getMapInfo($this->db);
});

$app->post('/getInfo',function($request,$response,$args){
    $data = $request->getParsedBody();
    $parsedata = [];
    $parsedata['id'] = filter_var($data['id'], FILTER_SANITIZE_STRING);
    return getInfo($this->db);
});

$app->post('/registerDevice', function($request,$response,$args){
    $data = $request->getParsedBody();
    $parsedata = [];
    $parsedata['id'] = filter_var($data['id'], FILTER_SANITIZE_STRING);
    $parsedata['ssnit_id'] = filter_var($data['ssnit_id'], FILTER_SANITIZE_STRING);
    return registerDevice($parsedata['id'],$parsedata['ssnit_id'],$this->db);
});

$app->post('/getNotifications', function($request,$response,$args){

    return getAllNotification($this->db);
});

$app->post('/getSum', function($request,$response,$args){
    $data = $request->getParsedBody();
    $parsedata = [];
    $parsedata['id'] = filter_var($data['id'], FILTER_SANITIZE_STRING);
    return getSum($parsedata['id'],$this->db);
});







function getUserDetails($Id,$db)
{
    $data = array();
    $query = "SELECT * FROM users WHERE ssnit_id=?";
    $q = $db->prepare($query);
    $q->execute(array($Id));
    $results = $q->fetch(PDO::FETCH_ASSOC);
    if(count($results) > 0)
    {
        $data['firstname'] = $results['first_name'];
        $data['lastname'] = $results['last_name'];
        $data['phone'] = $results['phone'];
        $data['status'] = $results['status'];
        $data['othernames'] = $results['other_names'];
        $data['issueDate'] = $results['issueDate'];
        $data['sex'] = $results['sex'];
        $data['id'] = $results['ssnit_id'];
        $data['profile'] = $results['profile'];
        $data['profile_pic'] = $results['profile_pic'];

        return json_encode($data);
    }else
    {
        $data['status'] = "No Data";
        return json_encode($data);
    }
}

function getRecords($ssnit_id,$db)
{
    $Info  = array();
    $sum = 0;
    $data = array();
    $query = "SELECT * FROM records WHERE ssnit_id=?";
    $q = $db->prepare($query);
    $q->execute(array($ssnit_id));
    WHILE($results = $q->fetch(PDO::FETCH_ASSOC))
    {
        $sum = $sum + $results['amount'];
        $data['id'] = $results['id'];
        $data['company'] = $results['company'];
        $data['ssnit_id'] = $results['ssnit_id'];
        $data['amount'] = $results['amount'];
        $data['date'] = $results['date'];
        $data['percentage'] = $results['percentage'];
        $data['sum'] = $sum;

        $Info[] = $data;
    }


    return json_encode($Info);
}

function getSum($ssnit_id,$db)
{
    $Info  = array();
    $sum = 0;
    $data = array();
    $query = "SELECT * FROM records WHERE ssnit_id=?";
    $q = $db->prepare($query);
    $q->execute(array($ssnit_id));
    WHILE($results = $q->fetch(PDO::FETCH_ASSOC))
    {
        $sum = $sum + $results['amount'];
    }
    $data['sum'] = $sum;
    $Info[] = $data;
    return json_encode($Info);
}

function getMapInfo($db)
{
    $Info = array();
    $data = array();
    $query = "SELECT * FROM mapInfo";
    $q = $db->prepare($query);
    $q->execute(array());
    WHILE($results = $q->fetch(PDO::FETCH_ASSOC))
    {
        $data['office'] = $results['office'];
        $data['lat']= $results['lat'];
        $data['long'] = $results['long'];
        $data['status'] = $results['status'];

        $Info[] = $data;
    }

    return json_encode($Info);
}

function registerDevice($regId,$deviceId,$db)
{
//    $checkQuery = "SELECT * FROM gcm WHERE device=?";
//    $checkQ = $db->prepare($checkQuery);
//    $checkQ->execute($deviceId);
//    $results = $checkQ->fetch(PDO::FETCH_ASSOC);
//    if(empty($results))
//    {
//        $query = "INSERT INTO gcm(regId,device) VALUES (?,?)";
//        $q = $db->prepare($query);
//        $q->execute(array($regId,$deviceId));
//    }
//    else
//    {
//        $query = "UPDATE gcm SET regId=? WHERE device=?";
//        $q = $db->prepare($query);
//        $q->execute(array($regId,$deviceId));
//    }

    $query = "INSERT INTO gcm(regId,device) VALUES (?,?)";
    $q = $db->prepare($query);
    $q->execute(array($regId,$deviceId));


    return "1";
}

function getInfo($db)
{
    $Info  = array();
    $data = array();
    $query = "SELECT * FROM info";
    $q = $db->prepare($query);
    $q->execute(array());
    WHILE($results = $q->fetch(PDO::FETCH_ASSOC))
    {
        $data['id'] = $results['id'];
        $data['info'] = $results['info'];
        $data['status'] = $results['status'];
        $data['date'] = $results['date'];
        $Info[] = $data;
    }

    return json_encode($Info);
}

function getAllNotification($db)
{
    $Info = array();
    $data = array();

    $query = "SELECT * FROM notification";
    $q = $db->prepare($query);
    $q->execute(array());
    WHILE($results = $q->fetch(PDO::FETCH_ASSOC))
    {
        $data['message'] = $results['message'];

        $Info[] = $data;
    }

    return json_encode($Info);
}



$app->run();
