<?php
/**
 * Created by IntelliJ IDEA.
 * User: pakabah
 * Date: 18/04/2016
 * Time: 9:14 PM
 */

class login
{

    function userLogin($username,$password,$db)
    {
        $query = "SELECT * FROM portal_users WHERE username=? AND password=?";
        $q = $db->prepare($query);
        $q->execute(array($username,$password));
        $results = $q->fetch(PDO::FETCH_ASSOC);
        if(count($results) > 0)
        {
            session_start();
            $_SESSION['username'] = $results['username'];
            $_SESSION['session_id'] = uniqid("SES");
            $_SESSION['name'] = $results['name'];
            return "1";
        }
        else{
            return "0";
        }
    }

    function userLogout()
    {
        unset($_SESSION['username']);
        unset($_SESSION['session_id']);
        unset($_SESSION['name']);
        session_destroy();
    }
}