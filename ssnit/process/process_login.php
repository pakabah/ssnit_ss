<?php
/**
 * Created by IntelliJ IDEA.
 * User: pakabah
 * Date: 18/04/2016
 * Time: 9:14 PM
 */
session_start();
include("../connection/config.php");
include("../includes/func_login.php");

global $db;

$postdata = file_get_contents("php://input");
$dataObj = json_decode($postdata,false);

if($dataObj->login)
{
    $username = $dataObj->username;
    $password = $dataObj->password;
    $app = new login();
    echo $app->userLogin($username,$password,$db);
}
elseif($dataObj->logout)
{
    $app = new login();
    $app->userLogout();
}
elseif($dataObj->getUsername)
{
    echo $_SESSION['name'];
}