var app = angular.module('app',['ngNewRouter','ngFileUpload']);

app.controller('RouteController', ['$router',function($router){
    $router.config([
        {path:'/',redirectTo: '/main'},
        {path:'/main', component: 'main'},
        {path:'/info',component:'info'},
        {path:'/records', component:'records'},
        {path:'/settings',component:'settings'},
        {path:'/users',component:'users'}
    ]);
}
]);

app.directive('datepicker', function() {
    return {

        restrict: 'A',
        // Always use along with an ng-model
        require: '?ngModel',

        link: function(scope, element, attrs, ngModel) {
            if (!ngModel) return;

            ngModel.$render = function() { //This will update the view with your model in case your model is changed by another code.
                element.datepicker({format: 'dd/mm/yyyy'});
                element.datepicker('update', ngModel.$viewValue || '');
            };

            element.datepicker().on("changeDate",function(event){
                scope.$apply(function() {
                    ngModel.$setViewValue(event.date);//This will update the model property bound to your ng-model whenever the datepicker's date changes.
                });
            });
        }
    };
});

app.directive('stringToNumber', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function(value) {
                return '' + value;
            });
            ngModel.$formatters.push(function(value) {
                return parseFloat(value, 10);
            });
        }
    };
});

app.factory('Login', function($http){
   return{
       userLogin: function(username,password)
       {
           return  $http.post('../ssnit/process/process_login.php',{login: '75INTDUDN7FUCHWW',username:username,password:password});
       },
       userLogout: function()
       {
           return  $http.post('../ssnit/process/process_login.php',{logout: '75INTDUDN7FUCHWW'});
       },
       getUsername: function()
       {
           return  $http.post('../ssnit/process/process_login.php',{getUsername: '75INTDUDN7FUCHWW'});
       }
   }
});

app.factory('Info',function($http){
   return{
       getAllInfo: function()
       {
           return  $http.post('../ssnit/process/process_info.php',{getAllInfo: '75INTDUDN7FUCHWW'});
       },
       getInfo: function(id)
       {
           return  $http.post('../ssnit/process/process_info.php',{getInfo: id});
       },
       deleteInfo: function(id)
       {
           return  $http.post('../ssnit/process/process_info.php',{deleteInfo: id});
       },
       updateInfo: function(id,status)
       {
           return  $http.post('../ssnit/process/process_info.php',{updateInfo: id,status:status});
       },
       saveInfo: function(info,date,profile)
       {
           return  $http.post('../ssnit/process/process_info.php',{saveInfo: info,date:date,profile:profile});
       },
       searchInfo: function(mSearch)
       {
           return  $http.post('../ssnit/process/process_info.php',{searchInfo: mSearch});
       }
   }
});

app.factory('Users', function($http){
   return{
       getAllUsers: function()
       {
           return  $http.post('../ssnit/process/process_users.php',{getAllUsers: '75INTDUDN7FUCHWW'});
       },
       searchUser: function(search)
       {
           return  $http.post('../ssnit/process/process_users.php',{searchUser: search});
       },
       deleteUser: function(id)
       {
           return  $http.post('../ssnit/process/process_users.php',{deleteUser: id});
       },
       chooseUser: function(id)
       {
           return  $http.post('../ssnit/process/process_users.php',{chooseUser: id});
       },
       saveUser: function(firstname,lastname,othername,phone,sex,issueDate,profile,orga)
       {
           return  $http.post('../ssnit/process/process_users.php',{saveUser: '75INTDUDN7FUCHWW',firstname:firstname,lastname:lastname,othername:othername,phone:phone,sex:sex,issueDate:issueDate,profile:profile,orga:orga});
       },
       updateUser: function(status,phone,ssnit_id,orga)
       {
           return  $http.post('../ssnit/process/process_users.php',{updateUser: '75INTDUDN7FUCHWW', status:status,phone:phone,ssnit_id:ssnit_id,orga:orga});
       },
       addCompany: function(companyName,status)
       {
           return $http.post('../ssnit/process/process_users.php',{addCompany: '75INTDUDN7FUCHWW', companyName:companyName,status:status})
       },
       getAllCompanies: function()
       {
           return $http.post('../ssnit/process/process_users.php',{getAllCompanies: '75INTDUDN7FUCHWW'})
       },
       deleteOrg: function(orgId)
       {
           return $http.post('../ssnit/process/process_users.php',{deleteOrg: orgId})
       }

   }
});

app.factory('Main', function($http){
    return{
        saveNotification: function(sendNotification)
        {
            return  $http.post('../ssnit/process/process_notification.php',{sendNotification: sendNotification});
        }
    }
});

app.factory('Records', function($http){
   return{
       saveRecord: function(ssnit_id,name,company,amount,recordDate)
       {
           return  $http.post('../ssnit/process/process_records.php',{insertRecord: '75INTDUDN7FUCHWW', ssnit_id:ssnit_id,name:name,company:company,amount:amount,date:recordDate});
       },
       getAllRecords: function()
       {
           return  $http.post('../ssnit/process/process_records.php',{getRecords: '75INTDUDN7FUCHWW'});
       },
       searchRecords: function(mSearch)
       {
           return  $http.post('../ssnit/process/process_records.php',{searchRecords: mSearch });
       },
       deleteRecord: function(id)
       {
           return  $http.post('../ssnit/process/process_records.php',{deleteRecord: id});
       },
       getName: function(name)
       {
           return  $http.post('../ssnit/process/process_records.php',{getName: name});
       },
       getAllCompanies: function()
       {
           return  $http.post('../ssnit/process/process_records.php',{getCompanies: '75INTDUDN7FUCHWW'});
       }
   }
});

app.controller('MenuController', function($scope,Login){
    Offline.check();
    $scope.logout = function()
    {
        Login.userLogout().success(function(data){

        });
    };

    Login.getUsername().success(function(data){
      $scope.username = data;
    })

});

app.controller('LoginController', function($scope,Login){

    console.log("Login Controller");
    $scope.login = function(username,password)
    {
        console.log("Login Pressed");
        Login.userLogin(username,password).success(function(data){
            console.log(data);
            if(data == "1")
            {
                window.open("/ssnit/ssnit/","_self");
            }else
            {

            }
        })
    }
});

app.controller('MainController', function($scope,Main){
$scope.nNotify = true;

    $scope.saveNotify = function(information)
    {
        Main.saveNotification(information).success(function(data){
           $scope.nNotify = false;
            $scope.information = "";
        });
    }
});

app.controller('RecordsController',function($scope,Records,$http){
    $scope.rSaved = true;
    $scope.cSaved = true;
    $scope.csvSaved = true;

    Records.getAllRecords().success(function(data){
        $scope.mRecords = data;
        console.log(data);
    });

    Records.getAllCompanies().success(function(data){
       $scope.mCompany = data;
    });

    $scope.saveRecord = function(ssnit_id,name,company,amount,recordDate)
    {
        Records.saveRecord(ssnit_id,name,company,amount,recordDate).success(function(data){
            console.log("Date " +recordDate);
            console.log(data);
            $scope.rSaved = false;
            $scope.ssnit_id = "";
            $scope.name = "";
            $scope.company = "";
            $scope.amount = "";
            $scope.recordDate = "";

            Records.getAllRecords().success(function(data){
                $scope.mRecords = data;
                console.log(data);
            });
        });

    };

    $scope.searchRecords = function(mSearch)
    {
        Records.searchRecords(mSearch).success(function(data){
            $scope.mRecords = data;
            console.log(data);
        });
    };

    $scope.deleteRecord = function(id)
    {
        Records.deleteRecord(id).success(function(data){
            console.log(data);
            Records.getAllRecords().success(function(data){
                $scope.mRecords = data;
            });
        })
    };

    $scope.findName = function(name)
    {
        Records.getName(name).success(function(data){
            $scope.data = data;
            $scope.name = $scope.data.name;
            $scope.company = $scope.data.org;
            console.log(data);
        })
    };



});

app.controller('SettingsController', function($scope){

});

app.controller('UsersController', function($scope,Users,Upload){

    $scope.cSaved = true;
    $scope.cCreated = true;
    $scope.sErr = true;

    Users.getAllUsers().success(function(data){
       $scope.mUsers = data;
    });

    Users.getAllCompanies().success(function(data){
       $scope.mCompany = data;
    });

    $scope.searchUser = function(search)
    {
        Users.searchUser(search).success(function(data){
            $scope.mUsers = data;
        })
    };

    $scope.chooseUser = function(id)
    {
        Users.chooseUser(id).success(function(data){
            $scope.paramData = data;
            $scope.mName = $scope.paramData[0].name;
            $scope.mSsnit_id = $scope.paramData[0].ssnit_id;
            console.log($scope.mSsnit_id);
        })
    };

    $scope.deleteUser = function(ssnit_id)
    {
        Users.deleteUser(ssnit_id).success(function(data){
            console.log(data);
            Users.getAllUsers().success(function(data){
                $scope.mUsers = data;
            });
        })
    };

    $scope.updateUser = function(status,phone,orga)
    {
        console.log(status+" "+phone);
        $scope.cSaved = true;
        if(phone == undefined)
        {
            phone = " ";
        }
        if(status == undefined)
        {
            status = " ";
        }
        Users.updateUser(status,phone,$scope.mSsnit_id,orga).success(function(data){
            console.log($scope.mSsnit_id);
            console.log(data);
            $scope.cSaved = false;
            Users.getAllUsers().success(function(data){
                $scope.mUsers = data;
            });
        })
    };

    $scope.saveUser = function(firstname,lastname,othername,phone,sex,issueDate,profile,orga)
    {

        $scope.cSaved = true;
        $scope.cCreated = true;
        $scope.sErr = true;
        if(othername == undefined || othername == null || othername === undefined || othername === null )
        {
            othername = " ";
        }
        if(phone == undefined || phone == null || phone === undefined || phone === null)
        {
            phone = " ";
            console.log("Empty phone "+ phone);
        }
        console.log(firstname,lastname,othername,phone,sex,issueDate,profile);
        //
        //if(isNaN(phone))
        //{
            $scope.cCreated = true;
            //Users.saveUser(firstname,lastname,othername,phone,sex,issueDate,profile,orga).success(function(data){
            //    console.log(data);
            //    $scope.cCreated = false;
            //    $scope.firstname = "";
            //    $scope.lastname = "";
            //    $scope.othername = "";
            //    $scope.phone = "";
            //    $scope.sex = "";
            //    $scope.issueDate = "";
            //    Users.getAllUsers().success(function(data){
            //        $scope.mUsers = data;
            //    });
            //});
        $scope.upload($scope.files,firstname,lastname,othername,phone,sex,issueDate,profile,orga);

        //}
        //else
        //{
        //    $scope.sErr = false;
        //}
    };

    $scope.addCompany = function(companyName,cStatus)
    {
        Users.addCompany(companyName,cStatus).success(function(data){
            Users.getAllCompanies().success(function(data){
                $scope.mCompany = data;
            })
        })
    };

    $scope.upload = function (files,firstname,lastname,othername,phone,sex,issueDate,profile,orga) {
        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                if (!file.$error) {
                    Upload.upload({
                        url: 'process/process_pic.php',
                        data: {
                            file: file,
                            firstname: firstname,
                            lastname: lastname,
                            othername: othername,
                            phone:phone,
                            sex: sex,
                            issueDate:issueDate,
                            profile:profile,
                            orga:orga
                        }
                    }).then(function (resp) {
                        //$timeout(function () {
                        //    $scope.log = 'file: ' +
                        //        resp.config.data.file.name +
                        //        ', Response: ' + JSON.stringify(resp.data) +
                        //        '\n' + $scope.log;
                        //});
                    }, null, function (evt) {
                        var progressPercentage = parseInt(100.0 *
                            evt.loaded / evt.total);
                        $scope.log = 'progress: ' + progressPercentage +
                            '% ' + evt.config.data.file.name + '\n' +
                            $scope.log;
                    });
                }
            }
            $scope.cCreated = false;
            $scope.firstname = "";
            $scope.lastname = "";
            $scope.othername = "";
            $scope.phone = "";
            $scope.sex = "";
            $scope.issueDate = "";
            Users.getAllUsers().success(function(data){
                $scope.mUsers = data;
            });
        }
    };


    $scope.deleteOrg = function(orgId)
    {
        Users.deleteOrg(orgId).success(function(data){
            Users.getAllCompanies().success(function(data){
                $scope.mCompany = data;
            });
        })
    }


});

app.controller('InfoController', function($scope,Info){

    $scope.iSaved = true;

   Info.getAllInfo().success(function(data){
       $scope.mInfo = data;
   });

    $scope.getInfo = function(id)
    {
        Info.getInfo(id).success(function(data){
           $scope.paramInfo = data;
            $scope.infoId = $scope.paramInfo[0].id;

        });
    };

    $scope.deleteInfo = function(id)
    {
      Info.deleteInfo(id).success(function(data){
          Info.getAllInfo().success(function(data){
              $scope.mInfo = data;
          });
      });
    };

    $scope.updateInfo = function(status,id)
    {
        Info.updateInfo(id,status).success(function(data){
            Info.getAllInfo().success(function(data){
                $scope.mInfo = data;
            });
        })
    };

    $scope.saveInfo = function(info,date,profile)
    {
        Info.saveInfo(info,date,profile).success(function(data){
            $scope.iSaved = false;
            $scope.infor = "";
            $scope.date = "";
            Info.getAllInfo().success(function(data){
                $scope.mInfo = data;
            });
        })
    };

    $scope.searchInfo = function(mSearch)
    {
        Info.searchInfo(mSearch).success(function(data){
            $scope.mInfo = data;
        })
    }

});